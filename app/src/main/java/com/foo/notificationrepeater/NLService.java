package com.foo.notificationrepeater;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.PowerManager;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.util.Log;

import java.util.Calendar;
import java.util.Date;

public class NLService extends NotificationListenerService {

    private String TAG = this.getClass().getSimpleName();

    private int mNumAlertsRemaining;
    private Handler mHandler;

    @Override
    public void onCreate() {
        super.onCreate();

        mHandler = new Handler();

        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    boolean shouldDoStuff() {


        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        if( !prefs.getBoolean(MainActivity.PREF_ENABLED, true)) {
            Log.d(TAG, "Should not do stuff because not enabled!" );
            return false;
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        int hour = cal.get(Calendar.HOUR_OF_DAY);

        boolean quietTimeEnabled = prefs.getBoolean(MainActivity.PREF_QUIET_TIME_ENABLED, true);

        if( quietTimeEnabled ) {

            int hourStart = Integer.parseInt( prefs.getString(MainActivity.PREF_QUIET_TIME_START, null) );
            int hourStop = Integer.parseInt( prefs.getString(MainActivity.PREF_QUIET_TIME_STOP, null) );

            Log.d(TAG, "Current hour = " + hour);

            if( hourStart < hourStop ) {
                if (hour >= hourStart && hour < hourStop ) {
                    Log.d(TAG, "Should not do stuff because of the hour (case 1)!");
                    return false;
                }
            }
            else {
                if (hour >= hourStart || hour < hourStop) {
                    Log.d(TAG, "Should not do stuff because of the hour (case 2)!");
                    return false;
                }
            }
        }

        // Maybe it makes more sense to just wait for the notification to get dismissed? In case the screen is randomly on?
        /*
        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        //if( pm.isInteractive() ) {
        //if( pm.isScreenOn() ) {
            Log.d(TAG, "Should not do stuff because phone is interactive!");
            return false;
        }
        */

        return true;
    }

    @Override
    public void onNotificationPosted(StatusBarNotification sbn) {

        Log.i(TAG, "**********  onNotificationPosted!!!");
        Log.i(TAG,"ID :" + sbn.getId() + "t" + sbn.getNotification().tickerText + "t" + sbn.getPackageName());

        if( isRelevant(sbn.getPackageName())) {
            startAlerts();
        }
    }

    boolean isRelevant( String name ) {
        if( !shouldDoStuff() ) {
            Log.d(TAG, "Not relevant because !shouldDoStuff()");
        }
        String[] relevantNames = new String[] {"line", "messenger", "sms", "facebook"};
        for( String s: relevantNames) {
            if( name.toLowerCase().contains(s) ) {
                Log.d(TAG, "Relevant!");
                return true;
            }
        }

        Log.d(TAG, "NOT Relevant because package name is not in list...");
        return false;
    }

    @Override
    public void onNotificationRemoved(StatusBarNotification sbn) {
        Log.i(TAG,"********** onNOtificationRemoved");
        Log.i(TAG, "ID :" + sbn.getId() + "t" + sbn.getNotification().tickerText + "t" + sbn.getPackageName());

        // Check that a relevant notification was removed, which indicates the user saw it.
        if( isRelevant(sbn.getPackageName())) {
            removeRunningAlerts();
        }
    }

    private void alertUser() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        int vibrateDuration = Integer.parseInt( prefs.getString(MainActivity.PREF_VIBRATE_DURATION, null) );

        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        v.vibrate(vibrateDuration);
    }

    Runnable mTimeUpdateRunnable = new Runnable() {
        public void run() {
            timeUpdate();
        }
    };

    void removeRunningAlerts() {
        if( mTimeUpdateRunnable != null ) {
            mHandler.removeCallbacks(mTimeUpdateRunnable);
        }
    }

    void startAlerts() {
        removeRunningAlerts();

        // TB TODO - Starting alert delay vs. onging alert delay?
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        mNumAlertsRemaining = Integer.parseInt( prefs.getString(MainActivity.PREF_NUM_ALERTS, null) );
        int alertDelay = Integer.parseInt( prefs.getString(MainActivity.PREF_ALERT_DELAY, null) );

        mHandler.postDelayed(mTimeUpdateRunnable, alertDelay);
    }

    void timeUpdate() {

        if( !shouldDoStuff() ) {
            Log.d(TAG, "Stopping timeUpdate because shouldDoStuff() is now false");
            return;
        }

        alertUser();

        mNumAlertsRemaining--;
        if( mNumAlertsRemaining > 0 ) {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
            int alertDelay = Integer.parseInt( prefs.getString(MainActivity.PREF_ALERT_DELAY, null) );

            mHandler.postDelayed(mTimeUpdateRunnable, alertDelay);
        }
    }

}

